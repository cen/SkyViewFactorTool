import os.path
import sqlite3 as sl
from typing import List

import geopy.distance
import numpy as np
from tqdm import tqdm

from tools.Database import database_columns
from tools.SVFRequestParams import SVFRequestParams
from tools.WriteReadme import write_readme


def calc_output_for_id(lat_lngs: List[str], pano_ids: List[str], cursor: sl.Cursor):
    """
    Given a list of panoIDs, retrieve their Sky, Tree and Building factors from the database and average them out.

    :param lat_lngs: Coordinate point or polygon associated with the ID in question.
    :param pano_ids: A list of panoIDs.
    :param cursor: sqlite3 database cursor.
    :return: [sky, tree, building] and a list of a panoIDs that actually had calculated Sky View Factor values.
    """

    lat_lngs_without_strings = [token for token in lat_lngs if "poly" not in token.lower()]
    lats = lat_lngs_without_strings[::2]
    lngs = lat_lngs_without_strings[1::2]
    center_coord = (sum(float(lat) for lat in lats) / len(lats), sum(float(lng) for lng in lngs) / len(lngs))

    associated_panos = []
    associated_svfs = []
    associated_distances_to_center = []
    failed_panos = []

    for pano_id in pano_ids:
        pano_entry = cursor.execute("SELECT * FROM skyviewfactors WHERE pano_id=?", (pano_id,)).fetchone()

        if pano_entry[1] == -1:
            print(
                f"Panoid {pano_id} was not calculated. It is possible it was removed from Google's servers"
                f" between fetching and SVF evaluation steps."
            )
            failed_panos.append(pano_id)
            continue

        associated_panos.append(pano_id)

        associated_svfs.append(pano_entry[1:4])

        distance_to_center = geopy.distance.distance(center_coord, pano_entry[5:7]).m

        associated_distances_to_center.append(distance_to_center)

    if not associated_svfs:
        return [-1, -1, -1], -1, -1, associated_svfs, failed_panos

    svf_array = np.array(associated_svfs)
    svf = np.average(svf_array, axis=0)

    min_distance = min(associated_distances_to_center)
    max_distance = max(associated_distances_to_center)

    return svf.tolist(), min_distance, max_distance, associated_panos, failed_panos


def output_cumulative_svfs(outfile: str, fail_file: str, cleaned_input: str, fetch_output_file: str, cursor: sl.Cursor):
    """
    Output file that has the cumulative SVF per location:
    "locationID lat lng sky tree    building    panoIDs"

    :param outfile: "finalResult.csv" as described above.
    :param fail_file: "failed_ids.txt" containing IDs whose panos could not be downloaded.
    :param cleaned_input: Cleaned task file from the Clean Input step.
    :param fetch_output_file: Output file from the Fetch Panos step.
    :param cursor: sqlite3 database cursor.
    """

    if os.path.isfile(outfile):
        os.remove(outfile)

    if os.path.isfile(fail_file):
        os.remove(fail_file)

    with open(fail_file, "w") as failf:
        failf.write("")

    params = SVFRequestParams()
    lat_lngs_by_id = {}

    amt_of_lat_lngs_per_id = 1

    with open(cleaned_input) as f:
        params.set_from_clean_file(f)

        for line in f:
            line_split = line.strip().split("\t")
            lat_lngs_by_id[line_split[0]] = line_split[1:]
            amt_of_lat_lngs_per_id = int(len(line_split[1:]) / 2)

    with open(fetch_output_file) as f:
        with open(outfile, "a") as outf:
            with open(fail_file, "a") as failf:
                outf.write(
                    "id,"
                    + ("lat,lng," * amt_of_lat_lngs_per_id)
                    + "sky,tree,building,minDistance,maxDistance,numberOfPanos,panoIDs\n"
                )

                amt_of_ids = sum(1 for _ in open(fetch_output_file))

                for line in tqdm(f.readlines(), desc=f"Outputting SVF Result: {outfile}", total=amt_of_ids):
                    line_split = line.strip().split("\t")

                    output_for_this_id, min_d, max_d, panos_for_this_id, failed_panos = calc_output_for_id(
                        lat_lngs_by_id[line_split[0]], line_split[1:], cursor
                    )

                    if failed_panos:
                        failf.write(line)

                    coord_id = line_split[0]

                    line_output = [coord_id]
                    line_output += lat_lngs_by_id[coord_id]
                    line_output += [str(x) for x in output_for_this_id]
                    line_output += [str(min_d), str(max_d)]
                    line_output.append(str(len(panos_for_this_id)))
                    line_output.append(" ".join(panos_for_this_id))

                    outf.write(",".join(line_output) + "\n")


def output_pano_info(outfile: str, all_panos_file: str, cursor: sl.Cursor):
    """
    Outputs a file with one line for every pano that was found and used in this task.
    Each line is of format: "panoID sky tree    building    panoDate    heading"

    :param outfile: Location of file to be output.
    :param all_panos_file: File containing every pano used, from the Evaluate SVF step.
    :param cursor: sqlite3 database cursor
    """
    if os.path.isfile(outfile):
        os.remove(outfile)

    with open(all_panos_file) as f:
        with open(outfile, "a") as outf:
            outf.write(",".join(database_columns.keys()) + "\n")

            problem_entries_all = set()

            amt_of_panos = sum(1 for _ in open(all_panos_file))
            desc = f"Outputting SVF Result: {os.path.basename(outfile)}"

            for line in tqdm(f.readlines(), desc=desc, total=amt_of_panos):
                pano_id = line.strip()
                pano_entry = cursor.execute("SELECT * FROM skyviewfactors WHERE pano_id=?", (pano_id,)).fetchone()

                problem_entries = [
                    list(database_columns.keys())[i]
                    for i, _ in enumerate(pano_entry)
                    if pano_entry[i] in {None, "UNKNOWN"} and len(database_columns) > i and pano_entry[1] != -1
                ]

                problem_entries_all.update(problem_entries)

                line = [str(x) for x in pano_entry]

                outf.write(",".join(line) + "\n")

    if problem_entries_all:
        print(f"Some pano entries were missing the following fields: {', '.join(sorted(problem_entries_all))}.")


def finalise_result(task_dir: str, cleaned_input: str, fetch_output_file: str, all_panos_file: str, database_file: str):
    """
    After all the processing steps, calculate average SVF from each location's found panoIDs and output two files.

    The first file is "finalResult.csv", which has lines of the following structure:
    "locationID lat lng sky tree    building    panoIDs"

    The second file is "individualPanoData.csv", which has lines of the following structure:
    "panoID sky tree    building    panoDate"

    :param task_dir: Working directory for the entire task.
    :param cleaned_input: Cleaned task file from "Clean Input" step.
    :param fetch_output_file: Fetch Panos output file.
    :param all_panos_file: File containing every pano used, from the Evaluate SVF step.
    :param database_file: Location of standard database file.
    """

    assert os.path.isfile(fetch_output_file)
    assert os.path.isfile(database_file)

    pano_data = os.path.join(task_dir, "individualPanoData.csv")
    output_file = os.path.join(task_dir, "finalResult.csv")
    failed_ids_file = os.path.join(task_dir, "idsWithFailedPanos.txt")
    readme_file = os.path.join(task_dir, "readme.txt")

    con = sl.connect(database_file)
    cursor = con.cursor()

    output_cumulative_svfs(output_file, failed_ids_file, cleaned_input, fetch_output_file, cursor)
    output_pano_info(pano_data, all_panos_file, cursor)
    write_readme(readme_file, cleaned_input, output_file, pano_data)

    cursor.close()
    con.close()
