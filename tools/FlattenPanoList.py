import os

from tqdm import tqdm


def flatten_pano_list(infile: str, outfile: str):
    """
    Collect all pano ids from a fetch output file (Where pano IDs are tied to location IDs and can be duplicated),
    then write them to a file.

    :param infile: "Fetch Panos" step output file to pull pano IDs from.
    :param outfile: File to write pano IDs to.
    """
    if os.path.isfile(outfile):
        os.remove(outfile)

    all_pano_ids = set()

    amt_of_lines = sum(1 for _ in open(infile))

    with open(infile) as f:
        for line in tqdm(f.readlines(), total=amt_of_lines, desc="Flattening pano list"):
            line = line.strip()
            all_pano_ids.update(line.split("\t")[1:])

    with open(outfile, "w") as f:
        f.writelines(pano + "\n" for pano in all_pano_ids)
