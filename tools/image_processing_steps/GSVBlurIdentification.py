import os
from functools import cache
from sqlite3 import Cursor

import keras
import numpy as np
from keras.models import load_model
from PIL import Image


@cache
def model() -> keras.Model:
    return load_model(os.path.join("tools", "image_processing_steps", "blur_model_448_V1.h5"))


def prediction(file):
    image = Image.open(file)
    image = image.resize((448, 448))
    image = np.asarray(image).astype(np.float32) / 255.0
    if image.shape != (448, 448, 3):
        image = np.delete(image, 3, 2)
    image = np.expand_dims(image, -1)
    image = np.expand_dims(image, 0)
    pred = model().predict(image)

    return pred[0][0]


def stitch_image(image_folder_path: str, pano_id: str):
    outfile_path = os.path.join(image_folder_path, "mosaic.png")

    tilesize = 512
    numtilesx = 4
    numtilesy = 2
    mosaicxsize = tilesize * numtilesx
    mosaicysize = tilesize * numtilesy
    # start_time = time.time()
    mosaic = Image.new("RGB", (mosaicxsize, mosaicysize), "black")
    blkpixels = 0
    for x in range(0, 4):
        for y in range(0, 2):
            img = Image.open(os.path.join(image_folder_path, f"{x}_{y}.jpg"))
            if y == 1:
                pix_val = list(img.getdata())
                blk1 = pix_val[tilesize * tilesize - 1]
                blk2 = pix_val[tilesize * (tilesize - 1)]
                blkpixels = blkpixels + sum(blk1) + sum(blk2)
            mosaic.paste(img, (x * tilesize, y * tilesize, x * tilesize + tilesize, y * tilesize + tilesize))
    xstart = int((512 - 128) / 2)
    xsize = mosaicxsize - xstart * 2
    ysize = mosaicysize - (512 - 320)
    if blkpixels == 0:
        mosaic = mosaic.crop((xstart, 0, xstart + xsize, ysize))
    mosaic = mosaic.resize((1024, 512))
    mosaic.save(outfile_path)
    return outfile_path


def sort_out_blur(chunk_directory: str, cursor: Cursor):
    assert os.path.isdir(chunk_directory)

    subtask_directories = next(os.walk(chunk_directory))[1]

    for subtask in subtask_directories:
        subtask_path = os.path.join(chunk_directory, subtask)
        for pano_pre_folder in (subdir for subdir in os.listdir(subtask_path) if subdir.startswith("pre_")):
            mosaic_path = stitch_image(os.path.join(subtask_path, pano_pre_folder), pano_pre_folder[4:])
            if prediction(mosaic_path) < 0.7:
                os.rename(
                    os.path.join(subtask_path, pano_pre_folder), os.path.join(subtask_path, pano_pre_folder) + "_BLUR"
                )

    assert False
