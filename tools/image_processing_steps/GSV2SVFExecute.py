import os
from sqlite3 import Cursor
from subprocess import DEVNULL, Popen


def execute_svf_batch(chunk_directory: str, gsv2svfbat: str, cursor: Cursor):
    """
    Evaluate Sky, Tree and Building factors for a list of panoIDs.
    This will split the list into a few sub-lists so GSV2SVF can be executed multiple times at once.
    For these sub-lists, functions will be called to create a working directory, then the images will be pre-downloaded.
    GSV2SVF will be called multiple times through Popen, then we wait for all processes to finish.
    The results will be extracted from the GSV2SVF results.txt file for each panoID, and those results will then be
    stored in the Sky View Factor database.

    :param chunk_directory: Path to the directory with the task files and predownloaded image files
    :param cursor: sqlite3 database cursor for panoIDs.
    :param gsv2svfbat: Absolute path to GSV2SVF batch script
    """
    assert os.path.isdir(chunk_directory)

    subtask_directories = next(os.walk(chunk_directory))[1]

    processes = []

    for subtask in subtask_directories:
        command = f'{gsv2svfbat} "{chunk_directory}/{subtask}"'

        processes.append(Popen(command, shell=True, stdout=DEVNULL, stderr=DEVNULL))

    for p in processes:
        p.wait()

    for subtask in subtask_directories:
        for root, dirs, files in os.walk(os.path.join(chunk_directory, subtask)):
            if "result.txt" in files:
                pano_id = os.path.basename(os.path.normpath(root))

                with open(os.path.join(root, "result.txt")) as f:
                    line = f.readline().strip()
                    sky_tree_building = [float(x) for x in line.split(",")]
                    sky = sky_tree_building[0]
                    tree = sky_tree_building[1]
                    building = sky_tree_building[2]

                cursor.execute(
                    "INSERT INTO skyviewfactors(pano_id, sky, tree, building) VALUES(?, ?, ?, ?)"
                    "ON CONFLICT(pano_id) DO UPDATE SET sky=?, tree=?, building=?",
                    (pano_id, sky, tree, building, sky, tree, building),
                )
