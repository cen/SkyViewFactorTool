import os
import random
import sqlite3 as sl
from collections import defaultdict
from itertools import islice
from typing import List

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from tqdm import tqdm
from webdriver_manager.chrome import ChromeDriverManager

from tools.SVFRequestParams import SVFRequestParams


def write_dates_to_database(fetch_panos_output: str, database_file: str):
    """
    Given the pano dates output from a fetch panos request, insert or update the date for each panoID into the database.

    :param fetch_panos_output: A string with lines of form "panoID\tpanoDate\tlat\tlng\n"
    :param database_file: Database file to write the data to
    """

    con = sl.connect(database_file)
    cursor = con.cursor()

    for line in fetch_panos_output.split("\n"):
        line = line.strip()

        if not line:
            continue

        line_split = line.split("\t")
        pano_id = line_split[0]
        date_string = line_split[1]
        lat = float(line_split[2])
        lng = float(line_split[3])
        heading = float(line_split[4])

        cursor.execute(
            "INSERT OR IGNORE INTO skyviewfactors(pano_id, date_taken, lat, lng, heading) VALUES(?, ?, ?, ?, ?)",
            (pano_id, date_string, lat, lng, heading),
        )

    con.commit()
    cursor.close()
    con.close()


def get_chrome_driver() -> webdriver:
    """
    Set up a webdriver for Google Chrome

    :return: Chrome Webdriver
    """
    chrome_path = ChromeDriverManager().install()
    if "THIRD_PARTY_NOTICES.chromedriver" in chrome_path:
        chrome_path = chrome_path.replace("THIRD_PARTY_NOTICES.chromedriver", "chromedriver")

    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    return webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)


def split_id_line_list_multipolygons(id_line_list: List[str]) -> List[str]:
    """
    Some areas might be multipolygons. In this case, their line looks like this:
    id  POLYSTART   lat1-1    lng1-1    ... POLYEND POLYSTART   lat2-1  lat2-2  ... POLYEND
    We have to split these up into several requests because the FetchPanos tool only supports single polygons.
    The way this is done is by making a new ID per polygon in the multipolygon, and denote them with a numeric prefix.
    Then, later, we will stitch the results of each polygon back together.

    :param id_line_list: List of IDs to multipolygons, tab separated.
    :return: List of new IDs to polygons, made from splitting the original list by polygon.
    """
    new_list = []
    for id_line in id_line_list:
        id_line_split = id_line.split("\t")
        geo_id, line_split = id_line_split[0], id_line_split[1:]
        individual_polys = []
        current_list = []

        for token in line_split:
            if token.lower() in {"polygonstart", "polystart"}:
                current_list = []
                continue

            if token.lower() in {"polygonend", "polyend"}:
                individual_polys.append(current_list)
                continue

            current_list.append(token)

        if current_list not in individual_polys:
            individual_polys.append(current_list)

        for i, poly in enumerate(individual_polys):
            new_list.append(f"{i!s}_{geo_id}\t" + "\t".join(poly))

    return new_list


def stitch_multipolys_back_together(fetchpanos_output: str) -> str:
    """
    See function "split_id_line_list_multipolygons": This function stitches the output back together.

    :param fetchpanos_output: Output of fetchpanos with single polygons
    :return: Stitched together fetchpanos output (polygons are merged again into their original ID)
    """
    id_lines = fetchpanos_output.split("\n")
    stitched_dict = defaultdict(lambda: set())

    for id_line in id_lines:
        if not id_line:
            continue
        id_line_split = id_line.split("\t")
        geo_id, id_line_split = id_line_split[0], id_line_split[1:]

        real_geo_id = "_".join(geo_id.split("_")[1:])

        stitched_dict[real_geo_id].update(id_line_split)

    return "\n".join(f"{geo_id}\t" + "\t".join(panos) for geo_id, panos in stitched_dict.items())


def use_n_random(temp_file: str, outfile: str, n: int):
    with open(temp_file) as original_output:
        total = sum(1 for _ in original_output)

    with open(temp_file) as original_output:
        with open(outfile, "w") as reduced_output:
            for line in tqdm(original_output, total=total, desc="Picking n random"):
                line_stripped = line.strip()
                if not line_stripped:
                    reduced_output.write(line)
                line = line_stripped

                geo_id_all_ids = line.split("\t")
                geo_id = geo_id_all_ids[0]
                all_ids = geo_id_all_ids[1:]
                new_ids = random.sample(all_ids, min(n, len(all_ids)))

                reduced_output.write("\t".join([geo_id, *new_ids]) + "\n")

    alternate_file_name = os.path.join(os.path.dirname(temp_file), "output_before_n_random.txt")
    os.rename(temp_file, alternate_file_name)



def fetch_panos(infile: str, outfile: str, step_directory: str, fetchpanos_html_path: str, database_file: str):
    """
    Fetch panos for a file containing request parameters and locations.

    :param infile: Cleaned task file with parameters and locations.
    :param outfile: Output file with locationIDs to panoIDs that were found around them.
    :param step_directory: Working directory for this processing step.
    :param fetchpanos_html_path: Location of the FetchLocations.html file that is used for the fetch pano requests.
    :param database_file: Database to write some of the fetched data to.
    """

    temp_file = os.path.join(os.path.dirname(outfile), "temp.txt")

    already_done_ids = set()

    if os.path.isfile(temp_file):
        with open(temp_file) as file:
            for line in file.readlines():
                if not line:
                    break
                already_done_ids.add(line.strip().split("\t")[0])

    params = SVFRequestParams()

    with open(infile) as f:
        params.set_from_clean_file(f)
        amt_of_ids = sum(1 for _ in f)

    with open(infile) as f:
        # Redundancy because of progress bar
        params.set_from_clean_file(f)
        bar = tqdm(f, desc="Fetch Panos", total=amt_of_ids)

        while True:
            next_id_chunk = [line.strip() for line in islice(bar, params.fetch_panos_chunk_size) if line.strip()]

            if not next_id_chunk:
                break

            # skip already done IDs
            next_id_chunk = [line for line in next_id_chunk if line.split("\t")[0] not in already_done_ids]
            if not next_id_chunk:
                continue

            multipolys = any("polystart" in line.lower() for line in next_id_chunk)
            if multipolys:
                next_id_chunk = split_id_line_list_multipolygons(next_id_chunk)

            next_ids = "\\n".join(next_id_chunk)

            driver = get_chrome_driver()
            driver.get("File:///" + os.path.abspath(fetchpanos_html_path))
            driver.implicitly_wait(20)
            script = f"""document.getElementById('latLons').value = '{next_ids}';
                     document.getElementById('links').checked = {str(params.links).lower()};
                     document.getElementById('simultaneousRequests').value = '{params.simultaneous_requests!s}';
                     document.getElementById('radius').value = '{params.radius!s}';"""
            if params.n_closest_images > 0:
                script += f"""document.getElementById('nClosestImages').value = '{params.n_closest_images!s}';"""
            driver.execute_script(script)

            start_button = driver.find_element(By.ID, "allWithinRadiusForAll")
            ids_out = driver.find_element(By.ID, "output")
            pano_dates_elem = driver.find_element(By.ID, "panoDates")

            WebDriverWait(driver, 20).until(lambda _: not start_button.get_attribute("disabled"))
            start_button.click()
            WebDriverWait(driver, 10000).until(lambda _: ids_out.get_attribute("value") != "")

            pano_dates_and_latlngs = pano_dates_elem.get_attribute("value")
            output = ids_out.get_attribute("value")

            driver.close()
            driver.quit()

            if multipolys:
                output = stitch_multipolys_back_together(output)

            with open(temp_file, "a") as f2:
                f2.write(output)

            write_dates_to_database(pano_dates_and_latlngs, database_file)

        if params.n_random != -1:
            use_n_random(temp_file, outfile, params.n_random)
        else:
            os.rename(temp_file, outfile)
