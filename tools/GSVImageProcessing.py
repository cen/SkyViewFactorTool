import logging
import os
import shutil
import sqlite3 as sl
from io import BytesIO
from multiprocessing import Pool
from sqlite3 import Cursor
from typing import List, Tuple

import requests
from requests.adapters import HTTPAdapter
from tqdm import tqdm
from urllib3 import Retry

from tools.Database import setup_database
from tools.image_processing_steps.GSV2SVFExecute import execute_svf_batch

# from tools.image_processing_steps.GSVBlurIdentification import sort_out_blur
from tools.SVFRequestParams import SVFRequestParams


def get_image(
    pano_id: str, x: int, y: int, zoom: int, folder_path: str, adapter: HTTPAdapter, session: requests.Session
):
    """
    Downloads a tile of a Google Street View pano from Google servers.

    In our case, this is zoom level 2, with 0 <= x <= 3 and 0 <= y <= 1, making a roughly 4:2 -> 2:1 image.
    This code is referenced from: https://github.com/jian9695/GSV2SVF/blob/master/SVFCore.py#L166
    - to ensure that the images will be in a format compatible with GSV2SVF.

    :param pano_id: ID of pano on Google's servers.
    :param x: x position of tile
    :param y: y position of tile
    :param zoom: zoom level for the panorama
    :param folder_path: path to save the tile to
    :param adapter: HTTP Adapter
    :param session: Requests Session
    """

    url = (
        f"https://geo{x%4}.ggpht.com/cbk?cb_client=maps_sv.tactile&authuser=0&hl=en"
        f"&panoid={pano_id}&output=tile&x={x}&y={y}&zoom={zoom}&nbt&fover=2"
    )
    outfile = f"{folder_path}/{x}_{y}.jpg"

    for i in range(1, 31):
        try:
            response = session.get(url)
            if response.status_code == requests.codes.ok:
                file = BytesIO(response.content)
                with open(outfile, "wb") as output:
                    output.write(file.getbuffer().tobytes())
                session.close()
                return

            logging.warning(f"GSV image request unexpectedly failed. This happens sometimes. Retrying... {i}/30")
        except ValueError:
            return

    session.close()
    logging.error("GSV image request unexpectedly failed, max retries exceeded!")


def download_all_tiles_for_one_picture(pano_id: str, zoom: int, folder_path: str):
    retry = Retry(total=200, backoff_factor=0.1)
    adapter = HTTPAdapter(max_retries=retry)

    session = requests.Session()
    session.mount("https://", adapter)

    for x in range(0, 4):
        for y in range(0, 2):
            get_image(pano_id, x, y, zoom, folder_path, adapter, session)

    session.close()
    adapter.close()


def pre_download_tiles(chunks_and_paths: List[Tuple[List[str], str]], params: SVFRequestParams):
    """
    Pre-download all the image tiles (8 tiles per image) for a set of images, in a format that GSV2SVF understands.
    This is done because GSV2SVF is slow about downloading images and doesn't work with some Antivirus systems as it
    uses an old Python 2 requests library. This function does it in a multi-threaded manner using the modern Python 3
    requests library.

    :param chunks_and_paths: List of chunks, with each chunk being a list of subdirectories and a chunk directory
    :param params: SVFRequestParams set by task file (or defaults)
    """
    args = []

    for chunk, folder_path in chunks_and_paths:
        for pano_id in chunk:
            pano_id_dir = os.path.join(folder_path, f"pre_{pano_id}")

            if not os.path.exists(pano_id_dir):
                os.makedirs(pano_id_dir)

            args.append((pano_id, 2, pano_id_dir))

    with Pool(params.simultaneous_img_predownload_threads) as p:
        p.starmap(download_all_tiles_for_one_picture, args)

    return


def make_chunk_folder(folder_path: str, chunk: List[str], params: SVFRequestParams):
    """
    Create folders and task file for a chunk in preparation for GSV2SVF execution.
    Calls pre_download_tiles (See above).

    :param folder_path: Root directory for sub-folders for each panoID.
    :param params: SVFRequestParams set by task file (or defaults)
    :param chunk: A list of panoIDs to download and evaluate images for.
    """
    os.makedirs(folder_path)

    task_file = os.path.join(folder_path, "task.csv")

    with open(task_file, "w") as f:
        f.write("\n".join(chunk))


def execute_batch(batch: List[str], step_directory: str, gsv2svfbat: str, params: SVFRequestParams, cursor: Cursor):
    task_subdirectory = os.path.join(step_directory, "currenttasks")

    if os.path.isdir(task_subdirectory):
        shutil.rmtree(task_subdirectory)

    os.makedirs(task_subdirectory)

    task_chunks = [batch[i : i + params.gsv2svf_task_size] for i in range(0, len(batch), params.gsv2svf_task_size)]

    chunks_and_paths = []

    for index, chunk in enumerate(task_chunks):
        folder_path = f"{task_subdirectory}/{index!s}"
        make_chunk_folder(f"{task_subdirectory}/{index!s}", chunk, params)
        chunks_and_paths.append((chunk, folder_path))

    pre_download_tiles(chunks_and_paths, params)

    if params.sort_blur:
        raise NotImplementedError("Sort blur mode does not currently work.")
        #  sort_out_blur(task_subdirectory, cursor)

    #  print(f"Executing svf batch with {len(batch)} pictures.")

    execute_svf_batch(task_subdirectory, gsv2svfbat, cursor)


def do_gsv_image_processing_steps(
    task_file: str, all_panos_file: str, outfile: str, step_dir: str, database_file: str, gsv2svfbat: str
):
    """
    Iterate all pano IDs that were found during the fetch step.
    If the database is already familiar with this panoID and has valid Sky, Tree and Building Factors associated,
    skip that pano.
    Otherwise, divide the pano IDs into chunks of around 10-100 (SIMULTANEOUS_INSTANCES * TASK_SIZE), then call
    the chunk-wise pano evaluation function in GSV2SVFExecute.py.

    :param task_file: Original task file.
    :param fetch_output: "Fetch panos" step output file with location IDs to their respective panos.
    :param outfile: Output file for this step, which has no relevant info to share, so it will just say "Done".
    :param step_dir: Working directory for this processing step.
    :param database_file: Absolute path of local panoID database file.
    :param gsv2svfbat: Absolute path of GSV2SVF bat script.
    """

    if os.path.isfile(outfile):
        os.remove(outfile)

    with open(task_file) as f:
        params = SVFRequestParams().set_from_clean_file(f)

    con = sl.connect(database_file)
    cursor = con.cursor()
    setup_database(cursor)

    next_batch = []

    with open(all_panos_file) as f:
        for line in tqdm(f, desc="Calculating SVF", total=sum(1 for _ in open(all_panos_file))):
            pano_id = line.strip()

            database_row = con.execute(
                "SELECT pano_id, sky FROM skyviewfactors WHERE pano_id = ?", (pano_id,)
            ).fetchone()

            if database_row and database_row[1] != -1:  # If already exists in database with valid value, skip
                continue  # TODO: Needs to be able to consider multiple factors

            next_batch.append(pano_id)

            if len(next_batch) == params.simultaneous_gsv2svf_instances * params.gsv2svf_task_size:
                execute_batch(next_batch, step_dir, gsv2svfbat, params, cursor)
                con.commit()
                next_batch = []
    if next_batch:
        execute_batch(next_batch, step_dir, gsv2svfbat, params, cursor)
        con.commit()

    con.commit()
    cursor.close()
    con.close()

    with open(outfile, "w") as f:
        f.write("Done")
