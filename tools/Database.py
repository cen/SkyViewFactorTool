import sqlite3 as sl

database_columns = {
    "pano_id": "TEXT NOT NULL UNIQUE PRIMARY KEY",
    "sky": "REAL NOT NULL DEFAULT -1",
    "tree": "REAL NOT NULL DEFAULT -1",
    "building": "REAL NOT NULL DEFAULT -1",
    "date_taken": 'TEXT NOT NULL DEFAULT "UNKNOWN"',
    "lat": "REAL DEFAULT NULL",
    "lng": "REAL DEFAULT NULL",
    "heading": "REAL DEFAULT NULL",
}


def setup_database(cursor: sl.Cursor):
    """
    Create skyviewfactors table in database if doesn't already exist.
    Add columns that weren't previously present.

    :param cursor: sqlite3 database cursor.
    """
    cursor.execute("""
            CREATE TABLE IF NOT EXISTS skyviewfactors (
                pano_id TEXT NOT NULL UNIQUE PRIMARY KEY,
                sky REAL NOT NULL DEFAULT -1,
                tree REAL NOT NULL DEFAULT -1,
                building REAL NOT NULL DEFAULT -1,
                date_taken TEXT NOT NULL DEFAULT "Unknown",
                lat REAL DEFAULT NULL,
                lng REAL DEFAULT NULL,
                heading REAL DEFAULT NULL
            );
    """)

    cursor.execute("PRAGMA table_info(skyviewfactors)")

    present_columns = {column[1] for column in cursor.fetchall()}

    for column_name, column_def in database_columns.items():
        if column_name not in present_columns:
            cursor.execute(f"""
            ALTER TABLE skyviewfactors ADD {column_name} {column_def}
            """)
