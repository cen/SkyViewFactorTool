import logging
from dataclasses import dataclass
from typing import List, TextIO


@dataclass
class SVFRequestParams:
    """
    Customisable parameters for Fetch Panos step
    """

    radius: int = 100  # Radius to search for panos in around the starting coordinate
    links: bool = True  # Whether to use the "links" parameter to find as many panos between sample points as possible
    fetch_panos_chunk_size: int = 30  # How many IDs per fetchpanos batch
    simultaneous_requests: int = 5  # How many requests to work on simultaneously (Each request uses 200 sub-request)
    simultaneous_gsv2svf_instances = 3  # How many instances of GSV2SVF run (Very CPU & RAM heavy, ~3 is recommended)
    gsv2svf_task_size = 10  # Chunk size for GSV2SVF executions (Lower is recommended so server requests can cool down)
    simultaneous_img_predownload_threads = 16  # Simultaneous threads for downloading GSV images. Recom.: # of CPU cores
    sort_blur = False  # Should the blursorter be used? Especially relevant for data in Germany. //TODO: NYI
    n_closest_images = -1  # Instead of finding all images in a radius, find the n closest. -1 -> Don't use this mode
    n_random = -1  # Instead of finding all images in a radius, use n random. -1 -> Don't use this mode

    def set_params_from_lines(self, input_lines: List[str]) -> "SVFRequestParams":
        """
        Given a list of strings each representing a line, try to parse out the parameters as best as possible.

        :param input_lines: List of lines that contain parameter definitions
        :return: self, after the adjustments were made
        """
        for line in input_lines:
            if not line:
                continue

            line_split = [token.strip() for token in line.split("=")]
            assert len(line_split) == 2, f"Could not evaluate parameter: '{line}'. Should be of form 'parameter=value'"

            parameter_name = line_split[0].lower().replace("_", "").replace("-", "")
            value = line_split[1]

            error_string = f"Don't understand value {value} for parameter {line_split[0]}"

            if parameter_name in {"radius"}:
                assert value.isnumeric(), error_string

                self.radius = int(float(value))
                continue
            if parameter_name in {"links", "uselinks"}:
                assert value.lower() in {"true", "1", "y", "yes", "n", "false", "0", "no"}, error_string

                self.links = value.lower() in {"true", "1", "y", "yes"}
                continue
            if parameter_name in {"simultaneousids", "simultaneousrequests"}:
                assert value.isnumeric(), error_string

                self.simultaneous_requests = int(value)
                continue
            if parameter_name in {"simultaneousgsv2svfinstances"}:
                assert value.isnumeric(), error_string

                self.simultaneous_gsv2svf_instances = int(value)
                continue
            if parameter_name in {"gsv2svftasksize", "svfthreads"}:
                assert value.isnumeric(), error_string

                self.gsv2svf_task_size = int(value)
                continue
            if parameter_name in {"simultaneousimgpredownloadthreads"}:
                assert value.isnumeric(), error_string

                self.simultaneous_img_predownload_threads = int(value)
                continue
            if parameter_name in {"sortblur", "blur"}:
                assert value.lower() in {"true", "1", "y", "yes", "n", "false", "0", "no"}, error_string

                self.sort_blur = value.lower() in {"true", "1", "y", "yes"}

                if self.sort_blur:
                    assert (
                        False
                    ), "The blursorter feature is still in development. It is not functional in its current state."
                continue
            if parameter_name in {"nclosestimages", "closestimages", "nclosest"}:
                assert value.lstrip("-").isnumeric(), error_string

                self.n_closest_images = int(value)

                if self.n_closest_images == 0 or self.n_closest_images < -1:
                    raise ValueError("Nclosest must be positive (or -1 to be disabled)")
                if self.n_random != -1 and self.n_closest_images != -1:
                    raise ValueError("Can't use both n_random and n_closest_images modes at the same time.")

                continue
            if parameter_name in {"nrandomimages", "randomimages", "nrandom"}:
                assert value.lstrip("-").isnumeric(), error_string

                self.n_random = int(value)

                if self.n_random == 0 or self.n_random < -1:
                    raise ValueError("Nrandom must be positive (or -1 to be disabled)")
                if self.n_random != -1 and self.n_closest_images != -1:
                    raise ValueError("Can't use both n_random and n_closest_images modes at the same time.")

                continue
            if parameter_name in {"fetchpanoschunksize"}:
                assert value.isnumeric(), error_string

                self.fetch_panos_chunk_size = int(value)

                continue

            logging.error(f"Not familiar with parameter {line_split[0]}")

        return self

    def set_params_from_string(self, input_string: str) -> "SVFRequestParams":
        """
        Given a string that may be made up of several lines seperated by \n, parse out parameters as best as possible.

        :param input_string: String containing parameter definitions separated by \n
        :return: self, after the adjustments were made
        """
        input_lines = input_string.strip().split("\n")

        return self.set_params_from_lines(input_lines)

    def set_from_clean_file(self, infile: TextIO) -> "SVFRequestParams":
        """
        Sets params from a clean .txt task file and advances the file generator to the start of the latlng section.

        :param infile: Clean task file (see CleanInput.py)
        :return: self, after the adjustments were made
        """

        param_lines = []

        while True:
            next_line = infile.readline().strip()
            if not next_line:
                break

            param_lines.append(next_line)

        return self.set_params_from_lines(param_lines)

    def output_as_string(self) -> str:
        """
        Output params as cleaned up, standardised string.

        :return: Standardised params string
        """

        return f"""radius={self.radius}
links={self.links}
fetch_panos_chunk_size={self.fetch_panos_chunk_size}
simultaneous_requests={self.simultaneous_requests}
simultaneous_gsv2svf_instances={self.simultaneous_gsv2svf_instances}
gsv2svf_task_size={self.gsv2svf_task_size}
simultaneous_img_predownload_threads={self.simultaneous_img_predownload_threads}
blur={self.sort_blur}
nclosestimages={self.n_closest_images}
nrandom={self.n_random}\n"""
