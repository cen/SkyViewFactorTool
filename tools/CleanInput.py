import re

from tqdm import tqdm

from tools.SVFRequestParams import SVFRequestParams


def clean_input(infile: str, outfile: str):
    """
    This function will try its best to parse any task file.
    :param infile: Absolute path to task file (.txt)
    :param outfile: Absolute target path to cleaned output file (.txt)
    :return: nothing
    """

    parameter_pattern = "^[a-zA-Z_-]+[ ]*=[ ]*[a-zA-Z0-9_-]*$"

    param_lines = []
    latlng_lines = []

    id_counter = 1

    with open(infile) as file:
        for line in tqdm(file.readlines(), total=sum(1 for _ in open(infile)), desc="Clean Input"):
            line = line.strip()

            if not line:
                # Empty line is ignored.
                continue

            if re.match(parameter_pattern, line):
                # Defines a request parameter
                param_lines.append(line)
                continue

            # Defines a latlng

            delimiter = "[\t,; ]+"

            # Comma delimiter:
            german_pattern = "([0-9,]+[;\t])+[0-9,]+$"
            if re.match(german_pattern, line):
                delimiter = "[\t;]"

            latlng_line = [token.replace(",", ".") for token in re.split(delimiter, line)]

            if not len(latlng_line) % 2:
                # No ID. Needs an ID.
                latlng_line.insert(0, f"ID_{id_counter}")
                id_counter += 1

            latlng_lines.append(latlng_line)

    parameters = SVFRequestParams()
    parameters.set_params_from_lines(param_lines)
    parameter_string = parameters.output_as_string()

    with open(outfile, "w") as file:
        file.write(parameter_string + "\n")

    with open(outfile, "a+") as file:
        file.writelines(["\t".join(line) + "\n" for line in latlng_lines])

    return
