import os
from dataclasses import dataclass, field
from typing import Dict, List, Tuple

from tqdm import tqdm

from tools.SVFRequestParams import SVFRequestParams


@dataclass
class ReadmeDefinition:
    amt_of_ids = 0
    vertices_per_polygon = -2
    final_result_fields: List[str] = field(default_factory=lambda: [])
    individual_pano_data_fields: List[str] = field(default_factory=lambda: [])


known_final_result_fields: Dict[str, str] = {
    "id": "The id defined for this location in the task file.",
    "lat": "The latitude of this location in WGS84.",
    "lng": "The longitude of this location in WGS84.",
    "lat1": "The latitude of the first vertex of this location polygon in WGS84. There may be fields lat2, lat3 etc.",
    "lng1": "The longitude of the first vertex of this location polygon in WGS84. There may be fields lng2, lng3 etc.",
    "sky": "The average Sky View Factor for Google Street View images found for this location (Domain: 0.0 - 1.0)."
    " IMPORTANT: This will be -1 if no images were found.",
    "tree": "The average Tree View Factor for Google Street View images found for this location (Domain: 0.0 - 1.0)."
    " IMPORTANT: This will be -1 if no images were found.",
    "building": "The average Building View Factor for Google Street View images found for this location"
    " (Domain: 0.0 - 1.0)."
    " IMPORTANT: This will be -1 if no images were found.",
    "minDistance": "The distance from this location to the closest Google Street View image that was found.",
    "maxDistance": "The distance from this location to the farthest Google Street View image that was used.",
    "numberOfPanos": "The amount of Google Street View images found for this location.",
    "panoIDs": "The 23-character unique IDs for the Google Street View images found for this location.",
}

known_individual_pano_fields: Dict[str, str] = {
    "pano_id": "The panoID of this Google Street View image (23-character string).",
    "sky": "The Sky View Factor for this Google Street View image (Domain: 0.0 - 1.0).",
    "tree": "The Tree View Factor for this Google Street View image (Domain: 0.0 - 1.0).",
    "building": "The Building View Factor for this Google Street View image (Domain: 0.0 - 1.0).",
    "date_taken": "The month & year of when this Goolge Street View image was taken.",
    "lat": "Latitude of this Google Street View image, using the WGS84 standard.",
    "lng": "Longitude of this Google Street View image, using the WGS84 standard.",
    "heading": "The angle in relation to true north of the center of this Google Street View image.",
}


def make_fields_output(fields: List[str], reference_dict: Dict[str, str]) -> str:
    output_string = ""

    unknown_fields = []
    for output_field in fields:
        if output_field in reference_dict:
            output_string += f"{output_field} - {reference_dict[output_field]}\n"
        else:
            unknown_fields.append(output_field)

    if unknown_fields:
        output_string += f"There were also the following other fields: {', '.join(unknown_fields)}\n"

    return output_string


def make_readme_def(cleaned_input: str, result_file: str, pano_file: str) -> Tuple[ReadmeDefinition, SVFRequestParams]:
    readme_definition = ReadmeDefinition()
    params = SVFRequestParams()

    with open(cleaned_input) as f:
        params.set_from_clean_file(f)
        len_params = sum(1 for _ in f)

    with open(cleaned_input) as f:
        params.set_from_clean_file(f)

        for line in tqdm(f, desc="Outputting Readme", total=len_params):
            line = line.strip()
            if not line:
                continue

            readme_definition.amt_of_ids += 1

            line_split = line.split("\t")
            amt_of_lat_lngs_per_id = int(len(line_split[1:]) / 2)

            if readme_definition.vertices_per_polygon == -2:
                readme_definition.vertices_per_polygon = amt_of_lat_lngs_per_id
            elif readme_definition.vertices_per_polygon != amt_of_lat_lngs_per_id:
                readme_definition.vertices_per_polygon = -1

    with open(result_file) as r:
        line = r.readline()
        line = line.strip()
        readme_definition.final_result_fields = line.split(",")

    with open(pano_file) as p:
        line = p.readline()
        line = line.strip()
        readme_definition.individual_pano_data_fields = line.split(",")

    return readme_definition, params


def write_readme(readme_file: str, cleaned_input: str, result_file: str, pano_data_file: str):
    if os.path.isfile(readme_file):
        os.remove(readme_file)

    readme_definition, params = make_readme_def(cleaned_input, result_file, pano_data_file)

    with open(readme_file, "a") as r:
        r.write("This file contains some information on how the data was generated and how to interpret it.\n")
        r.write("\n-----\n\n")

        r.write("WHAT IS THE SKY VIEW FACTOR?\n\n")
        r.write(
            "The Sky View Factor is the amount visible sky that is not blocked by trees or buildings.\n"
            "Specifically, for a 360° picture, it is the amount of pixels above the horizon that show the sky.\n"
            "(The projection of the 360° picture needs to be equal-area)\n"
            "Similarly, the Building View Factor is the amount of pixels showing buildings,"
            " and the Tree View Factor is the amount of pixels showing trees.\n"
            "Thanks to machine learning and automatic image processing, these metrics can be automatically"
            " extracted from 360° panoramic images.\n"
            "And thanks to Google Street View, we have a large catalogue of 360° panoramic images from all around"
            " the world.\n"
            "The SkyViewFactorTool is a free, fully functional, versatile solution for using large amounts of"
            " Google Street View image data to calculate the Sky View Factor at a desired location,"
            " as long as there is Google Street View coverage in the area you are interested in.\n"
        )

        r.write("\n-----\n\n")

        r.write("WHAT WAS THE INPUT FOR THIS TASK?\n\n")

        if params.n_closest_images > 0:
            closest_string = "exact closest Google Street View image"
            if params.n_closest_images > 1:
                closest_string = f"{params.n_closest_images} closest Google Street View images"

            r.write("Each line in the task file defines a location.\n")
            r.write(
                f'As the "n-closest" mode was used with the value {params.n_closest_images},'
                f" the SkyViewFactorTool will find the {closest_string} to each location.\n"
            )
        else:
            if readme_definition.vertices_per_polygon > 1:
                vertices = readme_definition.vertices_per_polygon
                vert_str = "a variable amount of" if vertices == -1 else str(vertices)
                r.write(
                    f"Each line in the task file defines a search area,"
                    f" which is a polygon with {vert_str} vertices.\n"
                )
                r.write(
                    "The SkyViewFactorTool will find Google Street View images inside each of these search areas.\n"
                )
            else:
                r.write("Each line in the task file defines a location.\n")
                r.write(
                    f"The SkyViewFactorTool will find Google Street View images around these locations"
                    f" in a radius of {params.radius} meters.\n"
                )
        r.write("Locations/Vertices are given as lat,lng pairs in the WGS84 standard.\n")

        if params.links:
            r.write(
                "The links parameter was set for this task,"
                " meaning all Google Street View images in the given search areas were searched for.\n"
            )
        else:
            r.write(
                "The links parameter was not set for this task, meaning a representative"
                " sample of Google Street View images were seached for in a symmetrical hexagon grid.\n"
                "This was likely a performance consideration due to a large data set or big search areas.\n"
            )

        r.write("\n-----\n\n")

        r.write("HOW THE RESULTS WERE ACQUIRED\n\n")

        r.write(
            "There are two major steps to the SkyViewFactorTool.\n\n"
            "1. Fetch Panos:\n\n"
            "In the first step,"
            " the SkyViewFactorTool makes coordinate-based calls to the Google Street View API"
            " to find Google Street View images.\n"
            'It does this by casting a sampling "net" (hexagon grid)'
            " and querying for Google Street View images at these locations.\n"
        )

        if params.links:
            r.write(
                "Since the links parameter was used, it then performs a Breadth-First-Search"
                " by querying the neightbors of each Google Street View image.\n"
            )

        r.write(
            '\nEach Google Street View image is uniquely identified by a 23-character string called a "panoID".\n\n'
        )

        r.write(
            "There are two outputs to the Fetch Panos step.\n"
            "The first output contains the panoIDs for the Google Street View images found in each search area"
            ' (by its ID in the task file). This will be the basis for the "finalResult.csv" output file.\n'
            "The other output has individual data for each Google Street View Image, saved in a local database."
            f" These parameters are: {', '.join(readme_definition.individual_pano_data_fields)}.\n\n"
        )

        r.write(
            "2. Evaluate SVF:\n\n"
            "In the second step, the GSV2SVF tool (https://github.com/jian9695/GSV2SVF) is used to segment each"
            " Google Street View image into its Sky, Tree, and Building portions.\n"
            "These Sky, Tree and Building values (Domain: 0.0 - 1.0) are stored in a database as well.\n\n"
        )

        r.write(
            "In a final step, all the acquired data is put together into two output files:\n"
            '- "finalResult.csv", containing the average Sky, Tree, and Building values for Google Street View'
            " images in the the search area defined by each line in the task file.\n"
            f"   This file has the fields: {', '.join(readme_definition.final_result_fields)}.\n"
            '- "individualPanoData.csv", containing individual data for each Google Street View image.\n'
            f"   This file has the fields: {', '.join(readme_definition.individual_pano_data_fields)}.\n"
        )

        r.write("\n-----\n\n")

        r.write("HOW TO INTERPRET THE DATA\n\n")

        r.write(
            'The main file of concern is "finalResult.csv".\n'
            "This file will mirror your task file, except now you have the"
            " Sky, Tree and Building values for each ID.\n"
            "This file contains the following fields:\n\n"
        )

        r.write(make_fields_output(readme_definition.final_result_fields, known_final_result_fields))

        r.write("\n")
        r.write(
            'The other file you might find useful is "individualPanoData.csv".\n'
            "This file contains individual data for every Google Street View image that was found and used"
            ' to generate the "finalResult.csv" output.\n'
            "Use this file in combination with finalResult.csv to calculate other statistical metrics"
            " (e.g. variance) or control for certain factors (e.g. tree leaf density in winter vs summer).\n"
            "This file contains the following fields:\n\n"
        )

        r.write(make_fields_output(readme_definition.individual_pano_data_fields, known_individual_pano_fields))
