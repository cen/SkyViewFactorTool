import importlib.util
import logging
import os.path
import platform
import sys
from dataclasses import dataclass
from pathlib import Path

from BaseClasses import ToolDefinition, ToolDefinitionArgs

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

modname = "SVF_Mod"
mod_dir = os.path.dirname(os.path.realpath(__file__))
fname = str(os.path.join(mod_dir, "SkyViewFactorTool.py"))

spec = importlib.util.spec_from_file_location(modname, fname)
if spec is None:
    raise ImportError(f"Could not load spec for module '{modname}' at: {fname}")
module = importlib.util.module_from_spec(spec)
sys.modules[modname] = module
try:
    spec.loader.exec_module(module)
except FileNotFoundError as e:
    raise ImportError(f"{e.strerror}: {fname}") from e


class SkyViewFactorToolClass(ToolDefinition):
    tool_name: str = "SkyViewFactorTool"
    metric_name: str = "Sky View Factor"
    supports_multipoly_mode = True

    @dataclass
    class SkyViewFactorToolArgs(ToolDefinitionArgs):
        radius: int = 200
        links: bool = True
        n_closest: int = -1

    @classmethod
    def can_be_used(cls):
        plt = platform.system()
        return plt == "Windows", f"{cls.tool_name} can only be used on Windows operating systems."

    @classmethod
    def supports_coordinate(cls, _1: float, _2: float) -> bool:
        #  SkyViewFactorTool supports coordinates from anywhere in the world.
        return True

    @classmethod
    def required_setup(cls, _: SkyViewFactorToolArgs):
        logging.warning("Automatic setup for SkyViewFactorTool has not been implemented.")
        return

    @classmethod
    def main(cls, args: SkyViewFactorToolArgs) -> Path:
        params_string = f"""radius={args.radius}
links={args.links}
n_closest={args.n_closest}"""

        new_file = os.path.join(mod_dir, "task.txt")

        if os.path.exists(new_file):
            os.remove(new_file)

        with open(args.task_file_location) as original_file:
            with open(new_file, "w") as new_task_file:
                new_task_file.write(params_string + "\n\n")
                for line in original_file:
                    new_task_file.write(line)

        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_directories_before = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_before = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        os.chdir(tool_dir)

        module.main([])

        os.chdir(correct_dir)

        output_directories_after = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_after = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        new_output_dirs = list(set(output_directories_after) - set(output_directories_before))
        assert len(new_output_dirs) == 1

        # TODO: Write readme

        return new_output_dirs[0]
