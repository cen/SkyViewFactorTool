# SkyViewFactorTool

_Part of the "EnvNeuro Python GIS Toolbox", a Python project for extracting multiple types of geodata from large datasets, for lists of coordinates from participants in EnvNeuro studies. This overarching project is currently in early theoretical stages, although individual tools like this one are already in a usable standalone state._

Tool to fetch Google Street View panoramas in a specified location and compute & average out their Sky View Factors, powered by [GSV2SVF](https://github.com/jian9695/GSV2SVF)

## Usage Requirements
1. You need a Google API Key with Street View and Maps APIs enabled. Please make sure you are familiar with any [costs](https://developers.google.com/maps/documentation/streetview/usage-and-billing) that may encur (As of November 2023, no costs encur as only Street View metadata requests & Maps Geometry Module calculations are performed using your API key).
2. You need to be running Windows. The [GSV2SVF-Tool](https://github.com/jian9695/GSV2SVF), which is used to analyse Panoramas for their Sky View Factor is currently not available on Linux.
3. You should use an Nvidia GPU. CPU-based classification is possible but takes roughly 10x as long.
4. You need to have Google Chrome installed on your computer.
5. You need Python 3.9 (maybe other versions around 3.9 are compatible)
6. You need an internet connection.

## How to use
1. Clone this repository and its submodules recursively. (e.g. "git clone --recurse-submodules [repo-link]")
2. Set up a Python environment and install requirements using pip. (e.g. "python -m pip install -r requirements.txt")
3. Run "python SkyViewFactorTool.py [taskfile]" - If you don't know how to make a task file yet, you can do "python SkyViewFactorTool.py example_task.txt". You can also look at this file for reference if you want to know how to make a task file.
4. When asked for your Google API key, provide it.
5. Wait. :) Try to be patient. Progress is reported sporadically but it can be a couple minutes between updates.

## Supported parameters
A task file can specify several parameters for how to search for and evaluate GSV images around your coordinates.

### Request Related
- **radius** - The radius to look for GSV images in.
- **links** - Whether to use GSV links to find more images. You should understand this as: "True" means every picture will be found in your desired radius, "False" means only a representative sample is used. You should consider using "links=False" if your dataset is very big.
- **nclosestimages** - Instead of finding images within a radius, find e.g. the 5 closest images. Overrides the radius parameter. It is advised to use this with "links" turned on.

### Performance Related
- **simultaneousrequests** - How many IDs are worked on at once. This number is to be multiplied with 200 to get the amount of requests that potentially could be sent to the Google Servers at once. Lower this if your browser is slow.
- **simultaneousgsv2svfinstances** - How many threads to run at the same time. This is essentially "how good is your PC". On a somewhat modern computer with a Ryzen2700X + RTX2080 with 16GB of RAM, the ideal value for this turns out to be 4.

### Parameters you don't need to mess with unless you have a reason to
- **svfthreads** - Chunk size for GSV2SVF step. Changing this has little effect.
- **simultaneousimgpredownloadthreads** - How many image download requests are made to the Google Servers at once. The default should be fine unless you have some sort of atypical internet connection.
