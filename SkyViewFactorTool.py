import os
import re
import shutil
import sqlite3 as sl
import sys
from datetime import datetime
from enum import Enum
from typing import Dict, FrozenSet, Set

from tools.CleanInput import clean_input
from tools.Database import setup_database
from tools.FetchPanos import fetch_panos
from tools.FinaliseResult import finalise_result
from tools.FlattenPanoList import flatten_pano_list
from tools.GSVImageProcessing import do_gsv_image_processing_steps


class SupportedArgs(FrozenSet[str], Enum):
    """
    Supported command-line arguments.
    """

    FIX_THIS_DIRECTORY = frozenset({"fix", "fixdirectory", "fixtask"})
    SKIP_SVF = frozenset({"skip", "skipsvf", "nosvf"})


class ProcessingStep(str, Enum):
    """
    The processing steps for a Sky View Factor task.
    "Clean input" means parsing a task file and re-outputting it in a standardised format.
    "Fetch panos" means finding all Street View pictures ("panos") in a specified radius.
    "Evaluate SVF" means using the GSV2SVF Tool to classify the found images into Sky, Tree and Building portions.
    """

    CLEAN_INPUT = "clean_input"
    FETCH_PANOS = "fetch_panos"
    FLATTEN_PANO_LIST = "flatten_pano_list"
    EVALUATE_SVF = "evaluate_svf"


class DefaultDirectories(str, Enum):
    """
    Important directories for different standard functions of the program.
    """

    WORKING_DIR = os.path.dirname(os.path.realpath(__file__))
    DATABASE_DIR = os.path.join(WORKING_DIR, "database")
    OUTPUTS_DIR = os.path.join(WORKING_DIR, "outputs")


class DefaultFiles(str, Enum):
    """
    Important files for different standard functions of the program.
    """

    FETCHPANOS_FILE = os.path.join(DefaultDirectories.WORKING_DIR, "tools", "fetchpanos", "FetchLocations.html")
    FETCHPANOS_API_KEY = os.path.join(DefaultDirectories.WORKING_DIR, "tools", "fetchpanos", "API_KEY.js")
    DATABASE_FILE = os.path.join(DefaultDirectories.DATABASE_DIR, "SVFDatabase.db")
    GSV2SVF_BATCH_SCRIPT = os.path.join(DefaultDirectories.WORKING_DIR, "tools", "GSV2SVF-reduced", "RunTaskSimple.bat")


def execute_step(step: str, task_file: str, task_directory: str, output_files: Dict[str, str]):
    """
    Execute one of the processing steps for the current task.

    :param step: The processing step to execute. Determines which function is going to be called.
    :param task_file: Task file containing parameters and latlngs.
    :param task_directory: Directory for files created during this processing step.
    :param output_files: Output files from the processing steps that have been finished so far.
    :return: Return the output file for this processing step.
    """
    step_directory = os.path.join(task_directory, step)
    step_outfile = os.path.join(step_directory, "output.txt")

    if os.path.isdir(step_directory):
        print(f"Step {step} already started.")
        if os.path.isfile(step_outfile):
            output_files[step] = step_outfile
            return
    else:
        os.makedirs(step_directory)

    if step == ProcessingStep.CLEAN_INPUT:
        clean_input(task_file, step_outfile)
    elif step == ProcessingStep.FETCH_PANOS:
        fetch_panos(
            output_files[ProcessingStep.CLEAN_INPUT],
            step_outfile,
            step_directory,
            DefaultFiles.FETCHPANOS_FILE.value,
            DefaultFiles.DATABASE_FILE.value,
        )
    elif step == ProcessingStep.FLATTEN_PANO_LIST:
        flatten_pano_list(
            output_files[ProcessingStep.FETCH_PANOS],
            step_outfile,
        )
    elif step == ProcessingStep.EVALUATE_SVF:
        do_gsv_image_processing_steps(
            output_files[ProcessingStep.CLEAN_INPUT],
            output_files[ProcessingStep.FLATTEN_PANO_LIST],
            step_outfile,
            step_directory,
            DefaultFiles.DATABASE_FILE.value,
            DefaultFiles.GSV2SVF_BATCH_SCRIPT.value,
        )
    else:
        assert False, f'Not familiar with step "{step}".'

    output_files[step] = step_outfile
    return


def stitch_together_outputs(old_directory: str, new_directory: str):
    old_results_filepath = os.path.join(old_directory, "finalResult.csv")
    old_pano_data_filepath = os.path.join(old_directory, "individualPanoData.csv")

    new_results_filepath = os.path.join(new_directory, "finalResult.csv")
    new_pano_data_filepath = os.path.join(new_directory, "individualPanoData.csv")

    old_task_filepath = os.path.join(old_directory, "task.txt")
    new_task_filepath = os.path.join(new_directory, "task.txt")

    stitched_results_filepath = os.path.join(new_directory, "stitchedResult.csv")
    stitched_pano_data_filepath = os.path.join(new_directory, "stitchedPanoData.csv")

    assert os.path.isfile(old_task_filepath)
    assert os.path.isfile(old_results_filepath)
    assert os.path.isfile(old_pano_data_filepath)
    assert os.path.isfile(new_task_filepath)
    assert os.path.isfile(old_results_filepath)
    assert os.path.isfile(old_pano_data_filepath)

    new_results_dict = {}
    with open(new_results_filepath) as new_results_file:
        for line in new_results_file.readlines():
            line = line.strip()
            if not line or line.startswith("id,"):
                continue
            new_results_dict[line.split(",")[0]] = line

    with open(old_results_filepath) as old_results_file:
        with open(stitched_results_filepath, "w") as stitched_results_file:
            for line in old_results_file.readlines():
                line = line.strip()
                if not line or line.startswith("id,") or line.split(",")[0] not in new_results_dict:
                    stitched_results_file.write(line + "\n")
                    continue

                stitched_results_file.write(new_results_dict[line.split(",")[0]] + "\n")

    new_pano_data_dict = {}
    with open(new_pano_data_filepath) as new_pano_data_file:
        for line in new_pano_data_file.readlines():
            line = line.strip()
            if not line or line.startswith("id,"):
                continue
            new_pano_data_dict[line.split(",")[0]] = line

    with open(old_pano_data_filepath) as old_pano_data_file:
        with open(stitched_pano_data_filepath, "w") as stitched_pano_data_file:
            for line in old_pano_data_file.readlines():
                line = line.strip()
                if not line or line.startswith("id,") or line.split(",")[0] not in new_pano_data_dict:
                    stitched_pano_data_file.write(line + "\n")
                    continue

                stitched_pano_data_file.write(new_pano_data_dict[line.split(",")[0]] + "\n")

    os.remove(new_task_filepath)
    os.remove(new_results_filepath)
    os.remove(new_pano_data_filepath)

    os.rename(old_task_filepath, new_task_filepath)
    os.rename(stitched_results_filepath, new_results_filepath)
    os.rename(stitched_pano_data_filepath, new_pano_data_filepath)


def fix_svf(task_file: str, task_directory: str, runtime_args: Set[SupportedArgs]):
    """
    For a given output of a finished task, see if there were any issues with it, and rectify them.

    :param task_file: Path to task file.
    :param task_directory: Path to task_directory that needs to be fixed.
    :param runtime_args: Runtime arguments that modify the behavior.
    """

    print("Warning: This mode is not fully tested yet.")

    previous_results_file = os.path.join(task_directory, "finalResult.csv")
    previous_pano_data_file = os.path.join(task_directory, "individualPanoData.csv")
    previous_task_file = os.path.join(task_directory, "clean_input", "output.txt")
    failed_ids_file = os.path.join(task_directory, "idsWithFailedPanos.txt")

    assert os.path.exists(task_directory), "The directory you're trying to fix doesn't exist."

    assert os.path.exists(previous_results_file), f"finalResult.csv not found in {task_directory}"
    assert os.path.exists(previous_pano_data_file), f"individualPanoData.csv not found in {task_directory}"
    assert os.path.exists(previous_task_file), f"task.txt not found in {task_directory}"

    assert os.path.exists(failed_ids_file), (
        f"idsWithFailedPanos.txt not found in {task_directory}. This output may"
        f" be older than the --fix feature. If that is the case, you can rerun"
        f" 'python SkyViewFactorTool.py {task_directory}' to generate it."
    )

    split_output_path = os.path.normpath(task_directory).split(os.sep)
    split_output_path[-1] += "_FIXED"

    new_output_dir = os.sep.join(split_output_path)
    new_task_file = os.path.join(new_output_dir, "task.txt")

    assert not os.path.isdir(new_output_dir), "There has already been an attempt to fix this directory."

    os.makedirs(new_output_dir)

    ids_that_need_to_be_redone = set()

    with open(failed_ids_file) as failed_ids:
        for line in failed_ids.readlines():
            line_split = line.strip().split("\t")
            if line_split:
                ids_that_need_to_be_redone.add(line_split[0])

    parameter_pattern = "^[a-zA-Z_-]+[ ]*=[ ]*[a-zA-Z0-9_-]*$"

    with open(previous_task_file) as old_task_f:
        with open(new_task_file, "a") as new_task_f:
            for line in old_task_f.readlines():
                line = line.strip()

                if not line:
                    new_task_f.write("\n")
                    continue

                if re.match(parameter_pattern, line):
                    # Defines a request parameter
                    new_task_f.write(line + "\n")
                    continue

                if line.split("\t")[0] in ids_that_need_to_be_redone:
                    new_task_f.write(line + "\n")

    do_svf(new_task_file, new_output_dir, runtime_args - {SupportedArgs.FIX_THIS_DIRECTORY})

    stitch_together_outputs(task_directory, new_output_dir)


def do_svf(task_file: str, task_directory: str, runtime_args: Set[SupportedArgs]):
    """
    For a task file, set up a working directory and execute all the processing steps in order.
    This calls functions from different submodules in "../tools".

    :param task_file: Path to task file.
    :param task_directory: Working directory for the task that is to be executed, will be created if it doesn't exist.
    :param runtime_args: Runtime arguments that modify the behavior.
    """
    assert os.path.isfile(task_file), (
        "Couldn't find a task file. Please either create a task.txt in the main"
        " directory, or pass it as the first launch argument."
    )

    if runtime_args:
        print(f"Running SkyViewFactorTool with args: {runtime_args}.")
    else:
        print("Running SkyViewFactorTool with no args.")

    con = sl.connect(DefaultFiles.DATABASE_FILE.value)
    cursor = con.cursor()
    setup_database(cursor)

    if SupportedArgs.FIX_THIS_DIRECTORY in runtime_args:
        fix_svf(task_file, task_directory, runtime_args)
        return

    if not os.path.exists(task_directory):
        os.makedirs(task_directory)

    # Make copy of task file, unless the directory already is a half-finished task
    task_file_copy = os.path.join(task_directory, "task.txt")
    if not os.path.isfile(task_file_copy) or not os.path.samefile(task_file, task_file_copy):
        shutil.copy(task_file, task_file_copy)

    output_files = {}

    for step in ProcessingStep:
        if step == ProcessingStep.EVALUATE_SVF and SupportedArgs.SKIP_SVF in runtime_args:
            print("Skipping SVF step. Done.")
            return

        execute_step(step, task_file, task_directory, output_files)

    finalise_result(
        task_directory,
        output_files[ProcessingStep.CLEAN_INPUT],
        output_files[ProcessingStep.FETCH_PANOS],
        output_files[ProcessingStep.FLATTEN_PANO_LIST],
        DefaultFiles.DATABASE_FILE.value,
    )


def ensure_api_key():
    assert os.path.isfile(DefaultFiles.FETCHPANOS_API_KEY.value)
    with open(DefaultFiles.FETCHPANOS_API_KEY.value, "r+") as f:
        line = f.read()

        if "API_KEY_HERE" in line:
            key = input("Please enter Google API Key with Maps and Street View API enabled, then press Enter.\n")
            key = key.strip()
            line = line.replace("API_KEY_HERE", key)

        f.seek(0)
        f.write(line)


def initial_setup():
    """
    Create all the important standard directories for this program if it is the first time it is executed.
    """
    for required_dir in DefaultDirectories:
        if not os.path.exists(required_dir):
            os.makedirs(required_dir)

    assert os.path.isfile(
        DefaultFiles.FETCHPANOS_FILE.value
    ), """Fetchpanos tool does not seem to be present. Please make
     sure you have all submodules installed, by downloading them manually or using these git commands:\n
     git submodule init\n
     git submodule update\n"""
    assert os.path.isfile(
        DefaultFiles.GSV2SVF_BATCH_SCRIPT.value
    ), """GSV2SVF tool does not seem to be present. Please make
     sure you have submodules installed, by downloading them manually or using these git commands:\n
     git submodule init\n
     git submodule update\n"""

    ensure_api_key()


def main(args):
    """
    Determine task file and task directory from arguments or use defaults, then start processing.
    """
    initial_setup()

    # Default task file: task.txt in main folder. Default task output directory: Current time.
    arg_task_file = os.path.join(DefaultDirectories.WORKING_DIR, "task.txt")
    arg_task_directory = os.path.join(DefaultDirectories.OUTPUTS_DIR, datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    parsed_runtime_args: Set[SupportedArgs] = set()

    for arg in args:
        if os.path.isfile(arg):
            arg_task_file = os.path.realpath(arg)
        elif arg.startswith("-"):
            arg = arg.lstrip("-").strip()
            arg_name = arg.lower().replace("_", "").replace("-", "")
            for possible_spellings in SupportedArgs:
                if arg_name in possible_spellings:
                    parsed_runtime_args.add(possible_spellings)
                    break
            else:
                print(f"Don't recognize runtime arg {arg}")
        else:
            head, tail = os.path.split(os.path.realpath(arg))
            if os.path.normpath(head) == os.path.normpath(DefaultDirectories.WORKING_DIR):
                head = os.path.join(head, "outputs")

            arg_task_directory = os.path.join(head, tail)

            potential_arg_task_file = os.path.join(arg_task_directory, "task.txt")
            if os.path.isfile(potential_arg_task_file):
                arg_task_file = potential_arg_task_file

    do_svf(arg_task_file, arg_task_directory, parsed_runtime_args)


if __name__ == "__main__":
    main(sys.argv[1:])
